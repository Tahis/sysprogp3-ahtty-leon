CC=gcc
CFLAGS=-c -I.
DEPS=point.h

all: calculadora

calculadora: main.o point.o
	$(CC) -lm -o calculadora main.o point.o -I.

%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm -f *.o calculadora
