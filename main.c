#include<stdio.h>
#include<point.h>
#include<stdlib.h>
int main(){
	float x1,y1,z1,x2,y2,z2,distancia;
	char xc[15],yc[15],zc[15];
	int formato;
	Point *punto1,*punto2;
	printf("Ingrese las medidas en metros\n");
	do
	{
		printf("Ingrese las coordenadas del primer punto (x y z)(123456.7890 234567.8901 345678.9012): ");
		scanf("%s %s %s",xc,yc,zc);
		formato=format(xc,yc,zc);
	}
	while(formato==0);
	x1=atof(xc);
	y1=atof(yc);
	z1=atof(zc);
	punto1=newPoint(x1,y1,z1);
	do
	{
		printf("Ingrese las coordenadas del segundo punto (x y z)(123456.7890 234567.8901 345678.9012): ");
		scanf("%s %s %s",xc,yc,zc);
                formato=format(xc,yc,zc);
	}
	while(formato==0);
	x2=atof(xc);
	y2=atof(yc);
	z2=atof(zc);
	punto2=newPoint(x2,y2,z2);
	distancia=calcularDistancia(punto1,punto2);
	printf("\nLa distancia entre los 2 puntos es %.4f metros\n",distancia);
}
